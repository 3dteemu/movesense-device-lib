swagger: '2.0'

info:
  version: NA
  title: Common types - Movesense-API
  description: |
    Commonly available types in addition to Whiteboard built-in types.
  x-api-type: public
  x-api-required: true

paths:
  x-workaround:
    x-get

definitions:

  VersionInfoArray:
    required:
      - versionInfo
    properties:
      versionInfo:
        type: array
        items:
          $ref: '#/definitions/VersionInfo'

  VersionInfo:
    required:
      - name
      - version
    properties:
      name:
        type: string
        minLength: 1
        maxLength: 16
      version:
        type: string
        minLength: 1
        maxLength: 16

  AddressInfo:
    required:
      - name
      - address
    properties:
      name:
        description: Address type (BLE, WIFI)
        type: string
        example: BLE
        minLength: 3
        maxLength: 16
      address:
        description: Address value
        type: string
        example: 80-1F-02-4E-F1-70
        minLength: 1
        maxLength: 16

  HRData:
    required:
      - average
      - rrData
    properties:
      average:
        description: |
            Heart rate value that is averaged and filtered and presented in Hz.
            Invalid heart rate is presented by 0.0f.
        type: number
        format: float
        x-unit: Hz
        minimum: 0.0    # equals in HR 0 bpm
        maximum: 4.2    # equals in HR 252 bpm
      rrData:
        description: |
            RR-interval returns all the intervals in milliseconds. By default,
            single rr-interval value is returned after each heart beat. If for
            some reason the rr-interval becomes longer than 60 sec (60000ms),
            then max values and remainder are added to array, until the real
            interval time is met. For example, rr-interval becomes
            2min 12 sec 310ms, then returned rrData array is [60000,60000,12310].
        type: array
        minItems: 1
        maxItems: 60
        items:
          type: integer
          format: uint16
          x-unit: ms
          minimum: 240    # equals in HR 250 bpm
          maximum: 60000  # equals in HR 1 bpm

  TimestampFromStartup:
    description: Milliseconds since the startup (e.g. like reset or so).
    type: integer
    format: uint64
    x-unit: millisecond

  MacAddress48:
    type: string
    description: IEEE 802 48-bit device MAC address
    example: 00:14:22:01:23:45
    minLength: 17
    maxLength: 17
